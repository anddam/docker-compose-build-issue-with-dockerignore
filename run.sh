#!/bin/sh


docker build --tag plainbuildtest .
docker-compose --project-name composebuildtest build

echo "Image content with plain docker:"
docker run --tty --interactive --rm plainbuildtest ls -1 --color=never | tee output-docker-plain.txt

echo "Image content with docker compose:"
docker run --tty --interactive --rm composebuildtest_app ls -1 --color=never | tee output-docker-compose.txt
