# Description

This repository is an SSCCE for replicate docker-compose ignoring
`.dockerignore` file directives when building.

Just run `sh run.sh` and check the content of `docker-plain.out` and
`docker-compose.out`.

docker-plain.out:

    baz

docker-compose.out:

    bar.excluded
    baz

